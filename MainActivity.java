package tftest.receiptdatacollector;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import org.json.JSONException;
import org.json.JSONObject;
import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "DataCollector";
    private static final int REQUEST_CODE = 1024;
    //
    private String mImagePath = null;
    private JSONObject mData = null;
    private int mWidth = 0, mHeight = 0;
    private float mLeft = 0, mTop = 0, mRight = 0, mBottom = 0;
    //
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                    Log.i(TAG, "OpenCV loaded successfully");
                    break;
                default:
                    super.onManagerConnected(status);
                    Log.i(TAG, "OpenCV " + status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE);
            }
        }
        //
        mImagePath = getIntent().getStringExtra("path");
        mWidth = getIntent().getIntExtra("width", 0);
        mHeight = getIntent().getIntExtra("height", 0);
        mLeft = getIntent().getFloatExtra("left", 0);
        mTop = getIntent().getFloatExtra("top", 0);
        mRight = getIntent().getFloatExtra("right", 0);
        mBottom = getIntent().getFloatExtra("bottom", 0);
        //
        Button apply = (Button) findViewById(R.id.button);
        apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                draw(mData);
            }
        });
        Button add = (Button) findViewById(R.id.button_add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = getCurrentOption();
                if (id == R.id.radio1) mData = adjustLeft(mData, 1);
                if (id == R.id.radio2) mData = adjustRight(mData, 1);
                if (id == R.id.radio3) mData = adjustTop(mData, 1);
                if (id == R.id.radio4) mData = adjustBottom(mData, 1);
                if (id == R.id.radio5) mData = adjustSplit(mData, 1);
                if (id == R.id.radio6) mData = nextKey(mData, 1);
                draw(mData);
            }
        });
        Button sub = (Button) findViewById(R.id.button_sub);
        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id = getCurrentOption();
                if (id == R.id.radio1) mData = adjustLeft(mData, -1);
                if (id == R.id.radio2) mData = adjustRight(mData, -1);
                if (id == R.id.radio3) mData = adjustTop(mData, -1);
                if (id == R.id.radio4) mData = adjustBottom(mData, -1);
                if (id == R.id.radio5) mData = adjustSplit(mData, -1);
                if (id == R.id.radio6) mData = nextKey(mData, -1);
                draw(mData);
            }
        });
    }

    private JSONObject adjustLeft(JSONObject data, int d) {
        String key = getCurrentBlock(data);
        try {
            data.optJSONObject(key).put("left", data.optJSONObject(key).optInt("left", 0) + d);
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private JSONObject adjustRight(JSONObject data, int d) {
        String key = getCurrentBlock(data);
        try {
            data.optJSONObject(key).put("right", data.optJSONObject(key).optInt("right", 0) + d);
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private JSONObject adjustTop(JSONObject data, int d) {
        String key = getCurrentBlock(data);
        try {
            data.optJSONObject(key).put("top", data.optJSONObject(key).optInt("top", 0) + d);
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private JSONObject adjustBottom(JSONObject data, int d) {
        String key = getCurrentBlock(data);
        try {
            data.optJSONObject(key).put("bottom", data.optJSONObject(key).optInt("bottom", 0) + d);
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private JSONObject adjustSplit(JSONObject data, int d) {
        String key = getCurrentBlock(data);
        try {
            data.optJSONObject(key).put("split", data.optJSONObject(key).optInt("split", 0) + d);
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private JSONObject nextKey(JSONObject data, int value) {
        int current = 0;
        List<String> list = new ArrayList<>();
        for (Iterator<String> it = data.keys(); it.hasNext(); ) {
            String key = it.next();
            if (key.startsWith("*")) current = list.size();
            list.add(key);
        }
        int next = (current + value + list.size()) % list.size();
        try {
            data.putOpt(list.get(current).replaceFirst("\\*", ""), data.remove(list.get(current)));
            data.putOpt("*" + list.get(next), data.remove(list.get(next)));
        } catch (JSONException e) {
            Log.d(TAG, "", e);
        }
        return data;
    }

    private String getCurrentBlock(JSONObject data) {
        for (Iterator<String> it = data.keys(); it.hasNext(); ) {
            String key = it.next();
            if (key.startsWith("*")) return key;
        }
        return null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                }
                return;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mData = detect();
        draw(mData);
    }

    private JSONObject detect() {
        if (TextUtils.isEmpty(mImagePath)) {
            Log.d(TAG, "detect path:" + mImagePath);
            return null;
        }
        File image = new File(mImagePath);
        if (!image.exists()) {
            Log.d(TAG, "detect image not exists!");
            return null;
        }
        JSONObject result = new JSONObject();
        putAreaData(result, "*boundary", mLeft, mTop, mRight, mBottom, 1);
        //
        Bitmap temp = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(image.getAbsolutePath()), mWidth, mHeight, true);
        //
        TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
        if (textRecognizer.isOperational()) {
            Frame imageFrame = new Frame.Builder()
                    .setBitmap(Bitmap.createBitmap(temp, (int) mLeft, (int) mTop, (int) (mRight - mLeft), (int) (mBottom - mTop)))
                    .build();
            SparseArray<TextBlock> textBlocks = textRecognizer.detect(imageFrame);
            Log.d(TAG, "detect textBlocks:" + textBlocks.size());
            for (int i = 0; i < textBlocks.size(); i++) {
                TextBlock block = textBlocks.get(textBlocks.keyAt(i));
                Log.d(TAG, "detect block:" + i + " components:" + block.getComponents().size());
                for (Text t : block.getComponents()) {
                    Log.d(TAG, "detect text:" + t.getValue() + " components:" + t.getComponents().size());
                    Rect rect = t.getBoundingBox();
                    putAreaData(result, t.getValue(), mLeft + rect.left, mTop + rect.top, mLeft + rect.right, mTop + rect.bottom, t.getValue().length());
                }
            }
        } else {
            Log.d(TAG, "detect textRecognizer.isOperational:" + textRecognizer.isOperational());
        }
        textRecognizer.release();
        return result;
    }

    private void draw(JSONObject data) {
        if (data == null) {
            Log.d(TAG, "draw data:" + data);
            return;
        }
        if (TextUtils.isEmpty(mImagePath)) {
            Log.d(TAG, "draw path:" + mImagePath);
            return;
        }
        File image = new File(mImagePath);
        if (!image.exists()) {
            Log.d(TAG, "draw image not exists!");
            return;
        }
        //
        Bitmap temp = Bitmap.createScaledBitmap(BitmapFactory.decodeFile(image.getAbsolutePath()), mWidth, mHeight, true);
        Bitmap bitmap = temp.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(bitmap);
        Paint red = new Paint();
        red.setColor(Color.RED);
        red.setStyle(Paint.Style.STROKE);
        red.setStrokeWidth(1.5f);
        Paint blue = new Paint();
        blue.setColor(Color.BLUE);
        blue.setStyle(Paint.Style.STROKE);
        blue.setStrokeWidth(1.5f);
        for (Iterator<String> it = data.keys(); it.hasNext(); ) {
            String key = it.next();
            JSONObject block = data.optJSONObject(key);
            if (block == null) continue;
            int left = block.optInt("left", 0);
            int top = block.optInt("top", 0);
            int right = block.optInt("right", 0);
            int bottom = block.optInt("bottom", 0);
            int split = block.optInt("split", 0);
            float dw = 1.0f * (right - left) / split;
            for (int i = 0; i < split; ++i)
                canvas.drawRect(left + i * dw, top, left + (i + 1) * dw, bottom, blue);
            if (key.startsWith("*")) canvas.drawRect(left, top, right, bottom, red);
            else canvas.drawRect(left, top, right, bottom, blue);
        }
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageBitmap(bitmap);
    }

    private JSONObject putAreaData(JSONObject o, String name, float left, float top, float right, float bottom, int split) {
        JSONObject data = new JSONObject();
        try {
            data.put("left", (int) left);
            data.put("top", (int) top);
            data.put("right", (int) right);
            data.put("bottom", (int) bottom);
            data.put("split", split);
            o.put(name, data);
        } catch (JSONException e) {
        }
        return o;
    }

    private int getCurrentOption() {
        RadioGroup group = (RadioGroup) findViewById(R.id.radioGroup);
        return group.getCheckedRadioButtonId();
    }
}
